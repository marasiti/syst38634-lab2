public class javaMethods {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Hello World");					
	}

	//Tim Marasigan
	public int smallest(int[] input) {
		
		int min = input[0];
		for(int i = 1; i < input.length; i++) {
			if(min > input[i])
				min = input[i];
		}
		
		return min;		
	}
	
	
	//Tim Marasigan
	public double average(int[] input) {
		
		double avg = 0;
		
		for(int i = 0; i < input.length; i++) {
			avg = avg + input[i];
		}
		
		avg = avg / input.length;
		
		return avg;
	}

	//Tim Marasigan
	public char middle(String input) {	
		int length = input.length();
		
		int mid = length / 2;
				
		return input.charAt(mid);		
	}
	
	//Tim Marasigan
	public int vowelCount(String input) {
		int count =  0;
		for(int i = 0; i < input.length(); i++) {
			if(input.charAt(i) == 'a' || input.charAt(i) == 'e' || input.charAt(i) == 'i' || input.charAt(i) == 'o' || input.charAt(i) == 'u')
				count++;
		}
		
		return count;
	}
	
	
	//Tim Marasigan
	public char charIndex(String input, int index) {
		return input.charAt(index);
	}
	
	
	//Tim Marasigan
	public int uniCount(String input) {
		int count = input.codePointCount(1, input.length());
		
		return count;
	}

}
